import Vue from 'vue';

import App from './App.vue';

import router from './router';
import apolloProvider from './vue-apollo';

import './registerServiceWorker';

Vue.config.productionTip = false;

new Vue({
  router,
  apolloProvider,
  render: (h: any): any => h(App),
}).$mount('#app');
