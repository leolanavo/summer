export interface User {
  id: string;
  name: string;
  email: string;
}

export interface UserInputData {
  email: string;
  name: string;
}

export interface UserInput {
  data: UserInputData;
}