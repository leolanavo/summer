import { User, UserInput } from 'src/resolvers/types';

async function getUser(email: string, context: any): Promise<User> {
  const userString: string =
    await context.redis.getAsync(email);

  const user: User = JSON.parse(userString);

  return user;
}

function createUser({ data }: UserInput, context: any): User {
  const user: User = { id: 'AHD1', ...data };

  context.redis.setAsync(data.email, JSON.stringify(user));

  return user;
}

export default {
  Query: {
    getUser: (_: any, args: any, context: any, ____: any): Promise<User> => {
      return getUser(args.email, context);
    },
  },
  Mutation: {
    createUser: (_: any, args: UserInput, context: any, ____: any): User => {
      return createUser(args, context);
    },
  },
};
