import Koa, { Context } from 'koa';

import Cors from '@koa/cors';

import { ApolloServer } from 'apollo-server-koa';
import { GraphQLSchema } from 'graphql';
import { importSchema } from 'graphql-import';
import { makeExecutableSchema } from 'graphql-tools';

import redis from 'src/redis';

import resolvers from 'src/resolvers';

const typeDefs: string = importSchema('src/graphql/schema.graphql');

const schema: GraphQLSchema = makeExecutableSchema({ typeDefs, resolvers });

const port: number = 3000;

const server: ApolloServer = new ApolloServer({
  schema,
  context: ({ ctx }: Context): any => ({
    ctx,
    redis,
  }),
});

const app: Koa = new Koa();
app.use(Cors());

server.applyMiddleware({ app });

app.listen({ port }, () =>
  console.log(`🚀 Server ready at http://localhost:${port}${server.graphqlPath}`),
);
